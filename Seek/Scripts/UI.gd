extends Control


onready var control = $HitsPanel/Control
var heart_full = preload("res://Assets/heart.png")
var heart_empty = preload("res://Assets/heart_void.png")


func _ready():
	pass # Replace with function body.


func update_health(value):
	for i in control.get_child_count():
		if (i <= value):
			control.get_child(i).texture = heart_full
		else:
			control.get_child(i).texture = heart_empty
