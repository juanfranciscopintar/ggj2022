extends Control


func _ready():
	$Buttons/BackButton.grab_focus()


func _input(_event):
	if (Input.is_action_pressed("ui_cancel")):
		if get_tree().change_scene("res://MainMenu.tscn") != 0:
			print("Weird")


func _on_BackButton_pressed():
	if get_tree().change_scene("res://MainMenu.tscn") != 0:
		print("Error loading main menu!")
