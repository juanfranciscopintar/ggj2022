extends Node2D

class_name Note

var speed: float = 120.0

signal note_escaped

# Called when the node enters the scene tree for the first time.
func _ready():
	if (self.connect("note_escaped", get_parent(), "_on_Note_escaped") != OK):
		print("Weird")


func _physics_process(delta):
	position.x -= delta * speed
	if (position.x < 450):
		emit_signal("note_escaped")
		queue_free() # Edit to start vanishing note at x = 512 and then queue_free()
