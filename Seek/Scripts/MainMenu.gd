extends Control


func _ready():
	$Buttons/StartButton.grab_focus()


func _on_StartButton_pressed():
	if get_tree().change_scene("res://Game.tscn") != 0:
		print("Error loading game!")


func _on_OptionsButton_pressed():
	if get_tree().change_scene("res://OptionsMenu.tscn") != 0:
		print("Error loading options menu!")


func _on_QuitButton_pressed():
	get_tree().quit()


