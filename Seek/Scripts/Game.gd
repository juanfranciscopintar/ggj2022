extends Node

# Network management constants
const PORT:int = 6009
const MAX_PLAYERS:int = 4

var client_id: int = -1

onready var note_spawn_pos = $NoteSpawnPosition.global_position
onready var ui = $UI
onready var conductor = $Conductor
onready var jekyll = $Jekyll
var jekyll_textures = [
	preload("res://Assets/jekyll_1.png"),
	preload("res://Assets/jekyll_2.png"),
	preload("res://Assets/jekyll_3.png"),
	preload("res://Assets/jekyll_miss.png")
]
var note = preload("res://Note.tscn")
var note_instance: Note
var note_speed: float
var on_hit_window: bool = false
var good_hits: int = 0
var hits_label: Label
var focus_note: Note
var control: int = 5
var last_note_was_double: bool = false
var last_four_notes: Array = []


func _ready():
	randomize()
	if (get_tree().connect("network_peer_connected", self, "_peer_connected") != OK):
		print("Error connecting signal")
	if (get_tree().connect("network_peer_disconnected", self, "_peer_disconnected") != OK):
		print("Error connecting signal")
	_start_Server()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	note_speed = (note_spawn_pos.x - $HitCircle.position.x) / conductor.seconds_per_beat
	conductor.play_with_beat_offset(4)
	hits_label = $UI/HitsPanel/HitsNumber


func _input(_event):
	if (Input.is_action_just_pressed("ui_select")):
		_check_hit()
	elif (Input.is_action_pressed("ui_cancel")):
		if get_tree().change_scene("res://MainMenu.tscn") != 0:
			print("Weird")


func _physics_process(_delta):
	ui.update_health(control)


func _update_control(value):
	control += value
	if (control < 1):
		if (client_id != -1):
			rpc_id(client_id, "switch_state")
		else:
			print("No client connected")
	print("Control: ", control)


func _on_Conductor_beat(position):
	$UI/BeatCounter.text = String(position)
	print("beat: ", position)


func _on_Conductor_measure(_position):
	jekyll.texture = jekyll_textures[randi() % 3]


func _spawn_note():
	note_instance = note.instance()
	add_child(note_instance)
	note_instance.position = note_spawn_pos
	note_instance.speed = note_speed
	last_note_was_double = false
	if (randi() % 4 == 0 and !last_note_was_double):
		last_note_was_double = true
		_spawn_note_with_offset()


func _spawn_note_with_offset():
	yield(get_tree().create_timer(conductor.seconds_per_beat / 2), "timeout")
	_spawn_note()


func _check_hit():
		if (on_hit_window == true): # good hit
			good_hits += 1
			hits_label.text = String(good_hits)
		else:
			if (control > 1):
				jekyll.texture = jekyll_textures[3] # miss
				_update_control(-1)
			else:
				_send_switch_mode()
		if (is_instance_valid(focus_note)):
			focus_note.queue_free()


func _on_NoteTimer_timeout():
	_spawn_note()


func _on_HitCircle_area_entered(area):
	$UI/BeatPopup.visible = true
	on_hit_window = true
	focus_note = area


func _on_HitCircle_area_exited(_area):
	$UI/BeatPopup.visible = false
	on_hit_window = false


func _on_Note_escaped():
	print("Note escaped")
	_update_control(-1)


# Networking functions
func _start_Server():
	var server = NetworkedMultiplayerENet.new()
	server.create_server(PORT,MAX_PLAYERS)
	get_tree().set_network_peer(server)
	print("Server up")


func _peer_connected(id:int): #cuando un cliente se conecta automáticamente lo agrega a un arreglo
	client_id = id


func _peer_disconnected(_id:int): #cuando se desconecta lo elimina
	client_id = -1


func _send_switch_mode(): # funcion a completar que manejaría el cambio de modo en el otro juego
	pass #rpc_id(clients_ids,"switch_state",....,args)
	#IMPORTANTE: ESTE NODO DEBE TENER EL MISMO NODEPATH QUE SU CONTRAPARTE EN EL OTRO JUEGO
	#caso contrario, la llamada no funciona. 
