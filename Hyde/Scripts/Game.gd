extends Node

# Network management constants
const PORT: int = 6009
const IP_ADDRESS: String = "192.168.0.39"

onready var conductor = $Conductor
export var tile_size: int = 5
var hyde = preload("res://Scenes/Hyde.tscn")
var girl = preload("res://Scenes/Girl.tscn")
var is_chasing:bool = false


func _ready():
	join_Server()
	
	if (get_tree().connect("connected_to_server", self, "_connected_ok") != OK):
		print("Error connecting signal")
	if (get_tree().connect("connection_failed", self, "_connection_fail") != OK):
		print("Error connecting signal")
	_spawn_hyde()
	_spawn_girl()
	conductor.play_with_beat_offset(4)
	
	


func _process(_delta):
	pass


func _spawn_hyde():
	$Level.add_child(hyde.instance())
	$Level/Hyde.translation = Vector3(-20.0-(tile_size/2.0), 4.2, -20.0-(tile_size/2.0))


func _spawn_girl():
	$Level.add_child(girl.instance())
	$Level/Girl.translation = Vector3(-9.0-(tile_size/2.0), 4.2, -9.0-(tile_size/2.0))


# Networking functions
func join_Server():
	var client = NetworkedMultiplayerENet.new()
	client.create_client(IP_ADDRESS,PORT)
	get_tree().set_network_peer(client)
	
func connected_ok():
	print("Connection successful!")

func connected_fail():
	print("Connection failed!")

remote func test_Signal():
	print("Signal recieved!")


func _on_Conductor_beat(position):
	print("Beat: ", position)


func _on_Conductor_measure(position):
	var rand_direction:Vector3
	
	rand_direction.x = rand_range(-1,1)
	rand_direction.z = rand_range(-1,1)
	
	print("Measure: ", position)
	
	if (is_chasing == false):
		if (position == 1):
			$Level/Hyde.move(rand_direction)
	else:
		$Level/Hyde.move($Level/Girl.translation)
		
	$Level/Girl.move()
