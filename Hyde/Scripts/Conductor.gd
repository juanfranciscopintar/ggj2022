extends AudioStreamPlayer

export var bpm : float = 120
export var measures := 4

var song_position := 0.0
var song_position_in_beats := 0
var seconds_per_beat := 60.0 / bpm
var last_beat:int = 0
var beats_before_start:int = 1
var measure = 4

onready var start_timer: Timer = $StartTimer

signal beat(position)
signal measure(position)


func _ready():
	seconds_per_beat = 60.0 / bpm


func _physics_process(_delta):
	if playing:
		song_position = get_playback_position() + AudioServer.get_time_since_last_mix() \
												- AudioServer.get_output_latency()
		song_position_in_beats = int(floor(song_position / seconds_per_beat)) + beats_before_start
		_report_beat()


func _report_beat():
	if last_beat < song_position_in_beats:
		if measure > measures:
			measure = 1
		emit_signal("beat", song_position_in_beats)
		emit_signal("measure", measure)
		last_beat = song_position_in_beats
		measure += 1


func play_with_beat_offset(offset_beats):
	beats_before_start = offset_beats
	start_timer.wait_time = seconds_per_beat
	start_timer.start()


#func play_from_beat(beat, offset):
#	play()
#	seek(beat * seconds_per_beat)
#	beats_before_start = offset
#	measure = beat % measures


func _on_StartTimer_timeout():
	song_position_in_beats += 1
	if song_position_in_beats < beats_before_start - 1:
		start_timer.start()
	elif song_position_in_beats == beats_before_start - 1:
		start_timer.wait_time = start_timer.wait_time - (AudioServer.get_time_to_next_mix()
														+ AudioServer.get_output_latency())
		start_timer.start()
	else:
		play()
		start_timer.stop()
	_report_beat()
