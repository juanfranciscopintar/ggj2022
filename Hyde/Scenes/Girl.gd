extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()


func move():
	var x_or_z:float = rand_range(-1,1) 
	var direction:Vector3
	
	direction.x = rand_range(-1,1)
	direction.z = rand_range(-1,1)
	
	
	if (translation.x < -2.6 && direction.x < 0):
		direction.x = -1*direction.x
	
	if (translation.z < -2.6 && direction.z < 0):
		direction.z = -1*direction.z
		
	if (translation.x > +2.6 && direction.x > 0):
		direction.x = -1*direction.x
	
	if (translation.z > +2.6 && direction.z > 0):
		direction.z = -1*direction.z
	
	if (x_or_z > 0):
		translation.z += (abs(direction.z)/direction.z)*Globals.tile_size
	else:
		translation.x += (abs(direction.x)/direction.x)*Globals.tile_size
